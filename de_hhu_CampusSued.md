# HHU Campus

Some information about the HHU campus in Düsseldorf for
incoming foreign students and staff, in particular about
the STW-Düsseldorf Campus Süd residential hall (Wohnanlage).

## Introduction

The HHU campus is divided in Campus Nord, mostly for medical and related
institutes, and Campus Süd, mostly for natural sciences and humanities
institutes.

The campus is located in the south part of Düsseldorf between the areas
of Bilk (north), Südpark (north west) and Wersten (south west), and is
served on the west side by a number of bus routes, mostly to the Bilk
train station and town centre, and on the east side by two main tram
routes, one of which is direct to the main train station.

## Maps:

*   Yandex satellite:
    <https://yandex.com/maps/10408/dusseldorf/?l=sat&ll=6.799210%2C51.189681&z=16>
*   OpenStreetMap:
    <https://www.openstreetmap.de/karte.html?zoom=16&lat=51.18918&lon=6.79556>
*   Google Map:
    <https://www.google.com/maps/@51.188928,6.7976581,17.25z>
*   Bing Maps: <https://www.bing.com/maps?q=51.188928+6.7976581>

## Facilities: Campus Nord

*   Pharmacy:
    *   Klinik Apotheke: in Moorenstrasse at the north edge of Campus
        Nord, closes at 8pm.
    *   Klinikum Apotheke: in the middle of Campus Nord, open 24 hours
        (may be reserved only to hospital use).

## Facilities: Campus Süd

*   There is a bookshop and a kiosk near the bridge between Campus Nord
    and Campus Süd.
*   Library:
    *   [University and regional library](https://www.ulb.hhu.de/nc/en/a-z.html?abc=s): in the
        middle of Campus Süd, access only by card.
    *   [Natural science library](https://www.ulb.hhu.de/ueberblick-gewinnen/verbund-und-fachbibliotheken/naturwissenschaften.html):
        In the bulding next to Informatik.
*   Food
    *   During partial lock-down only the UNO canteen (and the kiosk) is open,
        all cafeterias are closed.
    *   There is a Subway near the bridge between Campus Nord and Campus
        Süd, it is open late.
    *   There are cafetarias in most buildings. The one near 25.12 has a
        not-very-good reputation.
    *   One can buy a
        [MENSA charge card](https://www.stw-d.de/gastronomie/mensacard/)
        for UNO food and for other campus services like laundry,
        €2.50 for card; it can be charged at counter or
        [online](https://topup.klarna.com/stw_dusseldorf).

## Accomodation: [STW Campus Süd](https://goo.gl/maps/4BDvm7T3PxUWWNZh6)

*   Student village with single/twin bed flats and rooms.
*   Managed by [Studierendenwerk Düsseldorf](https://www.stw-d.de/).
*   Nearest food place is a pizza place.
*   The management office is in building 66, mezzanine floor.
*   Facilities:
    *   WiFi with ESSID `STW`.
    *   Individual postboxes.
    *   Locked bicyle storage in basement of Haus 66 (and some others).
    *   Laundries in the basement of numbers 64 and 72, with driers.
        Washing and drying paid by using the MENSA card.

Address while there:

    Haus [...] Raum [...] or Raum [...].[...]
    Wohnanlage Campus Süd, Universitätstrasse _[66..82]_
    Düsseldorf, NRW, D-40225, Germany

### Government details for longer term residents

*   If you stay for a longer period you must register as a resident with
    the Düsseldorf city council ("Anmeldung").
*   If you are paid by HHU, that is usually handled by the regional
    government office LBV, not by HHU, and dealing with them is not
    always straightforward and there can be long delays in getting
    the details right.
*   There are no local taxes, all taxes are national/federal.
*   For registered residents there is a mandatory tax of €17,50 (2021)
    per month for public radio and TV, and it is not optional.
*   Paying a part of your income into some health insurance plan and
    some pension plan is mandatory (this includes having an EHIC card
    from another EU country).

### Accomodation details

*   Buildings have a speakerphone for each flat, and each flat has also
    a buzzer next to the door.
*   Keys include a magnetic fob that gives access to the bicycle cellars
    and other building.
*   The curtains for the windows are not very dark so for those who need
    to sleep in the darkeness some kind of eyemask is a good idea.
*   Some flats have a cooker, some a microwave oven, all have a fridge.
*   The beds have a compartment under the mattress, and the mattress
    rests on a frame that can be pulled up with a strap.
*   When it is cold the heating elements should be set to number 3
    (which means a bit above 23C) and not lower to keep the walls warm.
*   The flats can get quite humid, but that usually can be fixed by
    opening the windows and letting in drier air from outside, and doing
    that is quite important.
*   There is a closet with a hoover in building 66 (and presumably in
    others).

### Links to official and unofficial further information pages

STW-D site:

*   [Requests for repairs](https://www.btsportal.de/stw/).

[Wohnanlage Campus Süd pages](https://wohnheim-campussued.de/):

*   [Contacts](https://wohnheim-campussued.de/kontakt/wichtige-kontaktdaten/):
    *   The administrative contact is for booking and payments and is at
        the STD-D office on campus.
    *   The management contact is for access and repairs and is on campus.
*   [Management office](https://wohnheim-campussued.de/2021/01/27/neue-hausverwaltung/).
*   Building information:
    *   [Security](https://wohnheim-campussued.de/ratgeber/sicherheit/).
    *   [Maintenance](https://wohnheim-campussued.de/ratgeber/wohnungspflege/).
    *   [Mail and parcels](https://wohnheim-campussued.de/ratgeber/post-pakete/).
    *   [Television](https://wohnheim-campussued.de/ratgeber/fernsehen/).
    *   [Internet sharing](https://wohnheim-campussued.de/ratgeber/internet-teilen/).
    *   [Internet connection](https://wohnheim-campussued.de/ratgeber/internet/).
    *   [Bicycles and storage](https://wohnheim-campussued.de/fahrraeder-und-fahrradkeller/).
*   Surrounding area information (in german):
    *   [Purchasing](https://wohnheim-campussued.de/around-us/einkaufen/).
    *   [Food and drink](https://wohnheim-campussued.de/around-us/essen-trinken/).
    *   [Cash machines](https://wohnheim-campussued.de/around-us/geldautomaten/).
    *   [Shops](https://wohnheim-campussued.de/around-us/shoppen/).

### Campus Süd [public transport](https://www.rheinbahn.de/)

Transport for the NRW region is run by
[VRR](https://www.vrr.de/de/tickets-tarife/tarifgebiete-regionen-preisstufen/),
the Düsseldorf area is specifically operated by
[RheinBahn](https://www.rheinbahn.de/).

The RheinBahn mobile app allows buying tickets online very conveniently.

Tickets are for:

*   [1 trip, 4 trips, 10 trips, or short periods](https://www.rheinbahn.de/tickets/gelegenheitsfahrer/)
    All trams and many buses have machines that can print these and
    accept credit and debit cards. The 10 trips ticket is only
    electronic via mobile phone.
*   [longer period tickets](https://www.rheinbahn.de/tickets/vielfahrer/).

Useful pages:

*   [Route map](https://www.vrr.de/fileadmin/user_upload/pdf/Stadtlinienplaene/Duesseldorf.pdf).
*   [Route map in other languages](https://www.rheinbahn.com/fahrplan/Seiten/Linienplan---verschiedene-Sprachen.aspx).
*   [Travel planner](https://www.rheinbahn.com/fahrplan/Seiten/timetable.aspx).
*   [Departure boards in realtime](https://www.rheinbahn.com/fahrplan/Seiten/departure_board.aspx).

### Bus and tram stops near campus

There are three bus stops on Universitätstrasse to the west and nord of
Campus Süd and a tram one to the north-west:

*   "Universität Mensa":
	[northbound](https://goo.gl/maps/SFUNFNPWa8F1wYSi8),
	[southbound](https://goo.gl/maps/avFxp1D4vQua6wib6).
*   "Universität Öst":
	[northbound](https://goo.gl/maps/9ht1cxKvAoRSZfY37),
	[southbound](https://goo.gl/maps/9ht1cxKvAoRSZfY37).
        The southbound side is only for arriving passengers.
*   "Universität West":
	[northbound](https://goo.gl/maps/cg7GecKYuCggr5Bm9),
	[southbound](https://goo.gl/maps/4eLR3dMDPUhbwgFdA)
*   "Universität Mitte":
	[northbound](https://goo.gl/maps/GMLSm1ANRAP4UpoF9),
	[southbound](https://goo.gl/maps/NywEoeKQgyfVvwoS6).
*   "Universität Süd":
	[northbound](https://goo.gl/maps/FWL5PTGWWgKXt6QKA),
	[southbound](https://goo.gl/maps/XPJcwpbVVnEpNmXi8).
        The one second nearest to the "Wohnanlage[, and the last stop
	for the 836.
*   "Universität Südost":
	[northbound](https://goo.gl/maps/dPHKKYUVpr71kGuC8),
	[southbound](https://goo.gl/maps/XPJcwpbVVnEpNmXi8).
        The one probably nearest to the "Wohnanlage".

### Bus and tram lines near campus

*   "Universität Öst" tram stop across the Botanic garden:
    -   U73 (to town centre):
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__0000689b.pdf).
    -   U79 (to central train station and Duisburg):
        [northbound-1](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00003148.pdf),
        [northbound-2](https://www.vrr.de/vrrstd3/AHF/20210723-113553/rbg/ah_70079__j21_R_1_18296.pdf).
    -   704 (to central train station and Derendorf):
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00007a3d.pdf).
*   "Universität Mitte" and "Universität Südost" bus stops for:
    -   731:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00003d09.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00000849.pdf).
    -   735:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00005531.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__000022d7.pdf).
    -   827:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00000388.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00001705.pdf).
    -   NE27:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__000031a3.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__0000595e.pdf).
    -   SB53:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00006e64.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__0000439e.pdf).
    -   SB57:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00005644.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__0000736f.pdf).
    -   835:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00003834.pdf),
        [souhtbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__0000179b.pdf).
    -   M3:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__000031df.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__000077e1.pdf).
*   "Universität Süd" and "Universität Mitte" bus stop for:
    -   836:
        [northbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00001f37.pdf),
        [southbound](https://www.vrr.de/vrrstd3/AHF/EFAMOBIL1__00007809.pdf).

## Retail: Campus Süd

### Shop areas

To the east of Campus Süd there is is the residential area of Wersten
and most of the nearby shops are in that area in particular in Kölner
Landstrasse.

To reach Wersten the Brückerbach has to be crossed and there are three
main bridges:

*   [North](https://www.google.com/maps/place/51°11'24.8"N+6°48'45.6"E/):
    vehicle and people bridge to the north part of Wersten.
*   [Middle](https://www.google.com/maps/place/51°11'10.2"N+6°48'17.6"E):
    people bridge with shortest path to mid-Wersten..
*   [South](https://www.google.com/maps/place/51°10'55.7"N+6°48'15.0"E/):
    people bridge to the south of the Sudöst bus stop.

In all three case the best walking route from Campus Süd is to first
reach [this point](https://www.google.com/maps/place/51°11'04.5"N+6°48'14.7"E/).

### Services

*   [AOK health insurance](https://www.hhu.de/internationales/internationale-forschende/welcome-center/formalitaeten/krankenversicherung/aok)
    ([map](https://goo.gl/maps/YbvLws2rrVP7PAyt6)).
*   [JUNO office for foreigners at HHU](https://www.hhu.de/internationales/internationale-forschende/welcome-center).
*   [TK health insurance HHU office](https://www.tk.de/service/app/2025530/filiale/sprechtag.app?tkcm=ab).
*   [ver.di labor union HHU branch](https://duessel-rhein-wupper.verdi.de/vor-ort/duesseldorf)

### Cash machines

*   Campus Nord:
    *   [Moorenstraße 5, 40225 Düsseldorf](https://goo.gl/maps/hpWkwt5oTaq5tZ488).
*   Himmelgeist:
    *   [Ickerswarder Str. 170-176, 40589 Düsseldorf](https://goo.gl/maps/f7HbyaCixkicPoUm9).
*   Kölner Landstrasse, north to south:
    *   [Kölner Landstraße 117, 40591 Düsseldorf](https://goo.gl/maps/k7E96Fos6X9JKwHc6).
    *   [Kölner Landstraße 172, 40591 Düsseldorf](https://goo.gl/maps/9PJE1xjeugMi36HM6).

### Main eating places

During partial lockdown they are mostly take-away only.

*   Campus Nord;
    *   [Subway](https://goo.gl/maps/NCbcrBTZvoFu4jJB6):
        Christophstraße 59, 40225 Düsseldorf.
    *   [Cafeteria Bistro UNO Mensa](https://goo.gl/maps/tFCLPQuAej73GYiz9):
        [Building 21.11](https://www.stw-d.de/gastronomie/oeffnungszeiten/).
*   Nearest:
    *   [Pizzeria Schmeltztiegel](https://g.page/schmelztiegel-pizzeria?share)
        Max-Born-Straße, 40591 Düsseldorf.
*   Kölner Landstrasse:
    *   [Alanya 2](https://goo.gl/maps/xmmSLW4ga9GUdDdS7) (kebabs):
        Kölner Landstraße 203-213, 40591 Düsseldorf.
    *   [Pizzeria Messina](https://g.page/PizzeriaMessinaDuesseldorf?share):
        Kölner Landstraße 285, 40589 Düsseldorf.
*   Himmelgeist:
    *   [Pizzeria Messina Due](https://goo.gl/maps/qFPNBmWDgNdYSjeW6):
        Steinkaul 10, 40589 Düsseldorf.

### Main shops

*   Bilk:
    *   [Kaufland](https://filiale.kaufland.de/service/filiale/duesseldorf-friedrichstadt-4193.html): Friedrichstraße 152,
40217 Düsseldorf
    *   [Arcaden](http://www.duesseldorf-arcaden.de/): Friedrichstraße 133, 40217 Düsseldorf
*   Nearest:
    *   Kiosk and [Deutsche Post filiale 496](https://goo.gl/maps/M3B3jmkno2gRsZNz5) (also
        [1](https://www.facebook.com/kiosk.wersten/),
        [2](https://alle-offnungszeiten.de/04011226/Deutsche_Post_Filiale_206_/_Kiosk_Wersten)),
        Otto-Hahn-Straße 37, 40591 Düsseldorf. Nice little corner shop
        with essentials, plus postal service and nearby postboxes.
*   Kölner Landstrasse, north to south:
    *   [TB-Computer](https://g.page/tb-computer-service?share):
        Kölner Landstraße 110, 40591 Düsseldorf. Computer parts shop.
    *   [Penny (supermarket)](https://goo.gl/maps/HjqYLv6Yr1eHQKeZA):
        Kölner Landstraße 121, 40591 Düsseldorf. Good selection,
        inexpensive, open until 22:00. Somewhat equivalent to TESCO in
        the UK.
    *   [KODi (housewares)](https://goo.gl/maps/aTYjfBy8cLNBkUd9A):
        Kölner Landstraße 174, 40591 Düsseldorf. Wide selection of
        useful household stuff, including  appliances and stationery,
        reasonable prices.
    *   [dm (drugstore)](https://goo.gl/maps/5Yq6WWsZBdpcftds5):
        Mergelgasse 1, 40591 Düsseldorf. Cosmetics, supplements, some
        common  medical things.
    *   [TEDi (poundshop)](https://goo.gl/maps/2y7m9r4yYepiu7KB6):
        Kölner Landstraße 189, 40591 Düsseldorf. Cheap stuff for home.
    *   [Hermes PaketShop](https://goo.gl/maps/3tFFQsEy6gjZw31w8);
        Kölner Landstraße 196, 40591 Düsseldorf. Send/receive parcels
        with Hermes instead of DeutschePost/DHL.
    *   [Aldi Süd (supermarket)](https://goo.gl/maps/7wSRpzZi2S8pTrdm7);
        Kölner Landstraße 203-213, 40591 Düsseldorf. Low prices,
        somewhat limited selection, decent quality.
    *   [Deutsche Post filiale 515](https://goo.gl/maps/3X1g3qquNG4rNyff8)
        Kölner Landstraße 202, 40591 Düsseldorf.
    *   [REWE (supermarket)](https://goo.gl/maps/efPcmCeE2ZwXmjhH7):
        Kölner Landstraße 203-213, 40591 Düsseldorf. Nicer or fancier
        products, higher prices except for "ja!" brand products.
        Somewhat equivalent to Sainsburys in the UK.
    *   [Apotheke in Wersten](https://goo.gl/maps/yJbuQSyCUcEt5B96A)
        Kölner Landstraße 205, 40591 Düsseldorf.
    *   [SEMA Supermarkt](https://goo.gl/maps/6pWFotMqbqKqBtQa8):
        Werstener Feld 3, 40591 Düsseldorf. Turkish/mediterranean food,
        fairly cheap, often in large cheaper packages (for example 3kg
        yoghurt tub).
*   Himmelgeist:
    *   [Aldi Süd (supermarket)](https://goo.gl/maps/3SCfmohYhvbZGwPw7)
        Ickerswarder Str. 172, 40589 Düsseldorf. . Low prices, somewhat
        limited selection but perhaps wider than the the on Kölner
        Landstraße, decent quality.

<!--
  ;;; Local Variables:
  ;;;   mode: markdown
  ;;;   eval: (auto-fill-mode 1)
  ;;;   eval: (filladapt-mode 1)
  ;;;   eval: (visual-line-mode 0)
  ;;;   eval: (whitespace-newline-mode 1)
  ;;;   truncate-lines:		t
  ;;;   indent-tabs-mode:	nil
  ;;;   fill-column:		72
  ;;;   tab-width:		8
  ;;; End:
  vim:set ft=markdown et nowrap nolbr ts=8 tw=72 sw=2:
-->
